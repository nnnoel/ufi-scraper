# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from urllib2 import urlopen
import re
import csv

# Company Name | Website | Email | Phone | Address

def main():
    def convert(input):
        if isinstance(input, dict):
            return {convert(key): convert(value) for key, value in input.iteritems()}
        elif isinstance(input, list):
            return [convert(element) for element in input]
        elif isinstance(input, unicode):
            return input.encode('utf-8')
        else:
            return input

    def get_member_ids(base, page, members, page_num):
        print 'Collecting [{}]: {}'.format(page_num, base + page)
        openurl = urlopen(base + page)
        soup = BeautifulSoup(openurl, 'html.parser')
        companies = soup.find_all('h3', class_='company-name')

        # Extract member profile ids from hrefs
        for c in companies:
            url = re.findall(n, str(c.a['href']))
            # print 'Adding {} to {}'.format(url, members)
            members += url

        return members

    def get_page_info(url):
        openurl = urlopen(url)
        soup = BeautifulSoup(openurl, 'html.parser')

        # Company Name
        company_name = [n.get_text().strip() for n in soup.find_all('h2', class_='company-name')]
        website = [w.p.get_text().strip() for w in soup.find_all('div', class_='company-contact-inline') if w.h4.get_text().strip() == 'Website']
        email = [w.p.get_text().strip() for w in soup.find_all('div', class_='company-contact-inline') if w.h4.get_text().strip() == 'Email']
        phone = [w.p.get_text().strip() for w in soup.find_all('div', class_='company-contact-inline') if w.h4.get_text().strip() == 'Phone']
        address = ' '.join(soup.find('h4', text='Address').find_next_sibling('p').get_text().split())

        # Row
        return {
            'Company Name': convert(''.join(company_name)),
            'Website': convert(''.join(website)),
            'Email': convert(''.join(email)),
            'Phone': convert(''.join(phone)),
            'Address': convert(address)
        }

    ## Main Directory
    base = 'http://www.ufi.org//membership/ufi-members/'
    page_num = 1
    p_max = 39
    page = 'search/?category=1&offset='+ str(page_num) +'&chapter&company&city&country'
    profile = base + 'member/'

    # Num regex
    n = r'\d+'

    members = []

    # Collect Member Profile IDs
    while page_num <= p_max:
        members = get_member_ids(base, page, members, page_num)
        page_num += 1
        page = 'search/?category=1&offset='+ str(page_num) +'&chapter&company&city&country'

    print 'Finished collecting profile IDs. Scraping..'

    # Begin scraping profiles
    entries = 0
    errors = 0
    with open('ufi-list.csv', 'w') as csvfile:
        w = csv.DictWriter(csvfile, fieldnames=['Company Name', 'Website', 'Email', 'Phone', 'Address'])
        w.writeheader()

        for m in members:
            url = profile + m
            row = get_page_info(url)

            try:
                w.writerow(row)
                entries += 1
                print 'Scraped: [{}]: {}'.format(entries, row)
            except Exception, e:
                print 'Error: ', e
                errors += 1

    print 'Done.. Scraped {} entries with {} errors'.format(entries, errors)


if __name__ == '__main__':
    main()